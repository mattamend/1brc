#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "hashtable.h"

bool is_empty(char *buf, size_t size)
{
	if (buf == NULL) {
		return true;
	}

	char zero[size];
	memset(&zero, 0, size);

	return memcmp(buf, zero, size) == 0;
}

struct table *ht_init(void)
{
	// Returning NULL is appropriate behavior, checked later
	struct table *ht = calloc(sizeof(struct table), HASH_SIZE);

	return ht;
}

void ht_free(struct table *ht)
{
	free(ht);
}

int find_index(char *key, uint8_t size)
{
	uint64_t hash = OFFSET;

	for (int i = 0; i < size; i++) {
		hash ^= (uint64_t) key[i];
		hash *= PRIME;
	}

	return (int)hash & (HASH_SIZE - 1);
}

void update_item(struct items *i, int32_t v)
{
	if (i->min == 0 || v < i->min) {
		i->min = v;
	}
	if (v > i->max) {
		i->max = v;
	}

	i->sum += v;
	i->count++;
}

void ht_insert(struct table *ht, char *key, uint8_t size, int32_t v)
{
	int i = 0;
	int index = find_index(key, size);

	// This should segfault at some point, but this is shouldn't happen
	// in a well-formatted measurements.txt. The segfault is the error tolerance.
	// Great program. I know.
	for (i = 0; !is_empty(ht[(index + 1) % HASH_SIZE].key, ht[(index + 1) % HASH_SIZE].size); i++) {
	}
	index += i;

	ht[index].key = key;
	ht[index].size = size;
	update_item(&ht[index].item, v);
}

struct items *ht_find(struct table *ht, char *key, uint8_t size)
{
	int i = 0;
	int index = find_index(key, size);

	// Like ht_insert(), this should also segfault at some point
	for (i = 0; memcmp(ht[index + i].key, key, size) != 0; i++) {
	}

	return &ht[index + i].item;
}
