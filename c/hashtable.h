#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <stdint.h>
#include <sys/types.h>

#define bool uint8_t
#define true 1
#define false 0

bool is_empty(char *buf, size_t size);

struct table {
	char *key;
	uint8_t size;
	struct items item;
};

#define HASH_SIZE 100000
#define OFFSET 14695981039346656037
#define PRIME 1099511628211

struct table *ht_init(void);
void ht_free(struct table *ht);
void ht_insert(struct table *ht, char *key, uint8_t size, int32_t v);
struct items *ht_find(struct table *ht, char *key, uint8_t size);


struct items {
	int32_t min, max, count;
	int64_t sum;
};

#endif /* HASHTABLE_H */