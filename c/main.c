#include <fcntl.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "hashtable.h"

struct table *ht;

struct file_map {
	void *ptr;
	size_t size;
};

struct file_map fm;

int station_count = 0;

int32_t parse_float(char *s, uint8_t size)
{
	int32_t n = 0;
	int i, len, j = 0;
	bool is_negative = false;

	if (s[0] == '-') {
		is_negative = true;
		s++;
		size--;
	}

	// Max possible length is -99.9, or 5 characters
	/// len = strnlen(s, 5);
	for (i = 0; i < size; i++) {
		if (s[i] >= '0' && s[i] <= '9') {
			j++;

			n += (s[i] - '0') * (int32_t)pow(10, size - j - 1);
		}
	}

	return is_negative ? -n : n;
}

void update_table(char *s, uint8_t len)
{
	struct table;
	int i = 0;
	bool float_section = false;
	uint8_t num_len = 0;

	for (i = 0; i < len; i++) {
		if (s[i] != ';') {
			if (float_section) {
				num_len++;
			}
		} else {
			len -= i;
			float_section = true;
		}
	}

	ht_insert(ht, s, len, parse_float(s + num_len));
}

void parse_file()
{
	char *s = fm.ptr;
	size_t i = 0;
	uint8_t len = 0;
	
	for (i = 0; i < fm.size; i++) {
		if (s[i] == '\n' || s[i] == '\0') {
			update_table(&s[i]++, len, ht);
			len = 0;
		} else {
			len++;
		}
	}
}

__attribute__((constructor)) void mem_init(void)
{
	ht = ht_init();
	if (ht == NULL) {
		perror("ht_init()");
		exit(1);
	}
}

__attribute__((destructor)) void mem_cleanup(void)
{
	ht_free(ht)
	// Normally we would check if it failed, but it's exiting anyway
	munmap(fm.ptr, fm.size);
}

struct file_map init(char *filename)
{
	void *ptr;
	int fd;
	struct stat sb;

	fd = open(filename, O_RDONLY);
	if (fd == -1) {
		perror("open");
		exit(1);
	}

	if (fstat(fd, &sb) == -1) {
		perror("fstat");
		exit(1);
	}

	ptr = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (ptr == MAP_FAILED) {
		perror("mmap");
		exit(1);
	}

	if (close(fd) == -1) {
		perror("close");
		exit(1);
	}

	fm.ptr = ptr;
	fm.size = sb.st_size;
}

int main(int argc, char *argv[])
{
	int i, empty = 0;
	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", argv[0]);
		return 1;
	}

	init(argv[1]);

	parse_file();

	return 0;
}
