use std::{collections::HashMap, env, fs};

struct Items {
    min: i32,
    max: i32,
    count: i32,
    sum: i64,
}

impl Items {
    fn new(v: i32) -> Items {
        Items {
            min: v,
            max: v,
            count: 1,
            sum: 0 + v,
        }
    }

    fn update(self, v: i32) -> Items {
        if self.min == 0 || v < self.min {
            self.min = v;
        }
        if v > self.max {
            self.max = v;
        }

        self.sum += v;
        i.count += 1;
    }
}

fn update_hashmap(hashmap: &mut Hashmap<String, Items>, line: String) {
    let v: Vec<&str> = line.split(';').collect();
    hashmap.entry(v[0]).or_insert();
}

fn main() {
    let fc = fs::read_to_string(os.args().next().next());

    let mut stations: HashMap<String, Items> = HashMap::with_capacity(100_000);

    for line in fc.lines() {
        update_hashmap(&mut stations, line);
    }
}